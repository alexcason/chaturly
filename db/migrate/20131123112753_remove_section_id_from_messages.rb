class RemoveSectionIdFromMessages < ActiveRecord::Migration
  def change
    remove_column :messages, :section_id
  end
end
