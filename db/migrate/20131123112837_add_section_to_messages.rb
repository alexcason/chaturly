class AddSectionToMessages < ActiveRecord::Migration
  def change
    change_table :messages do |m|
      m.references :section
    end
  end
end
