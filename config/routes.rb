Chaturly::Application.routes.draw do
  root 'home#index'

  resources :sections, only: [:show], :path => 'c' do
    resources :messages, only: [:index, :create]
    resources :analytics, only: [:index]
  end

  match 'search/feedback' => 'search#feedback', via: :get

  resources :search, only: [:index]

  resources :about, only: [:index]
end