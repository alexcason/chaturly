$(function () {

    var colors = Highcharts.getOptions().colors,
        categories = ['Postive Feedback', 'Negative feed back'],
        name = 'Growot.com',
        data = [{
            y: 90.0,
            color: colors[0],
            drilldown: {
                name: 'Postive feed back',
                categories: ['Looks great', 'I want one', 'gift idea', 'Good value'],
                data: [10.85, 7.35, 33.06, 2.81],
                color: colors[0]
            }
        }, {
            y: 10.0,
            color: colors[1],
            drilldown: {
                name: 'Negative feedback',
                categories: ['Looks great', 'I want one', 'gift idea', 'Good value'],
                data: [0.20, 0.83, 1.58, 13.12, 5.43],
                color: colors[1]
            }
        }];

    function setChart(name, categories, data, color) {
        chart.xAxis[0].setCategories(categories, false);
        chart.series[0].remove(false);
        chart.addSeries({
            name: name,
            data: data,
            color: color || 'white'
        }, false);
        chart.redraw();
    }

    var chart = $('#Chart1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Growot.com'
        },
        subtitle: {
            text: 'What is work for who? What is not working working and why.'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: 'Popularity'
            }
        },
        plotOptions: {
            column: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            var drilldown = this.drilldown;
                            if (drilldown) { // drill down
                                setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                            } else { // restore
                                setChart(name, categories, data);
                            }
                        }
                    }
                },
                dataLabels: {
                    enabled: true,
                    color: colors[0],
                    style: {
                        fontWeight: 'bold'
                    },
                    formatter: function () {
                        return this.y + '%';
                    }
                }
            }
        },
        tooltip: {
            formatter: function () {
                var point = this.point,
                    s = this.x + ':<b>' + this.y + '% market share</b><br/>';
                if (point.drilldown) {
                    s += 'Click to view ' + point.category + ' versions';
                } else {
                    s += 'Click to return';
                }
                return s;
            }
        },
        series: [{
            name: name,
            data: data,
            color: 'white'
        }],
        exporting: {
            enabled: false
        }
    })
    .highcharts(); // return chart
});



$(function () {
    $('#Chart2').highcharts({

        chart: {
            type: 'bubble',
            plotBorderWidth: 1,
            zoomType: 'xy'
        },

        title: {
            text: 'Sentiments/Demographics'
        },

        xAxis: {
            gridLineWidth: 1,
            title: {
                text: 'Sentiment'
            }
        },

        yAxis: {
            startOnTick: false,
            endOnTick: false,
            title: {
                text: 'Age groups'
            }
        },


        series: [{
            name: "Positive",
            data: [
                [10, 2, 63],
                [20, 70, 89],
                [30, 60, 73],
                [40, 80, 14],
                [50, 40, 20],
                [60, 30, 34],
                [70, 10, 53],
                [80, 20, 70],
                [90, 4, 28]
            ],
            marker: {
                fillColor: {
                    radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
                    stops: [
                        [0, 'rgba(255,255,255,0.5)'],
                        [1, 'rgba(69,114,167,0.5)']
                    ]
                }
            }
        }, {
            name: "Negative",
            data: [
                [10, 15, 2],
                [20, 30, 10],
                [30, 40, 50],
                [40, 50, 80],
                [50, 80, 40],
                [60, 20, 20],
                [70, 20, 10],
                [80, 14, 5],
                [90, 4, 5]
            ],
            marker: {
                fillColor: {
                    radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
                    stops: [
                        [0, 'rgba(255,255,255,0.5)'],
                        [1, 'rgba(170,70,67,0.5)']
                    ]
                }
            }
        }]

    });
});


var Counter = 0;
$(function () {

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    // Create the chart
    $('#Chart3').highcharts('StockChart', {
        chart: {
            events: {
                load: function () {

                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(function () {
                        var x = (new Date()).getTime(); // current time
                        Counter = Counter + Math.round(Math.random() * 100000);
                        series.addPoint([x, Counter], true, true);
                    }, 2000);
                }
            }
        },

        rangeSelector: {
            buttons: [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: false,
            selected: 0
        },

        title: {
            text: 'Growat - Interest'
        },

        exporting: {
            enabled: false
        },

        series: [{
            name: 'Random data',
            data: (function () {
                // generate an array of random data
                var data = [], time = (new Date()).getTime(), i;
                
                var incrementreset = 0;
                var increment = 0;
                for (i = -100; i <= 0; i++) {
                    if (incrementreset == 5) {
                        increment = increment + increment+ 1;
                        incrementreset = 0;
                    }
                    incrementreset = incrementreset + 1;

                    Counter = Counter + increment + Math.round(Math.random() * 10);
                    data.push([
                        time + i * 1000,
                        Counter
                    ]);
                }
                return data;
            })()
        }]
    });

});

