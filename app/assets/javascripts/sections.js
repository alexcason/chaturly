var chaturly = { }

function sectionsInit() {
  scrollToBottom();

  //window.setInterval(loadMessages, 2000);
}

function scrollToBottom() {
  $(".main-inner").scrollTop($(".main-inner")[0].scrollHeight);
}

function loadMessages() {
  // load new messages and append to chat
  var getPath = window.location.pathname + '/messages';

  $.getJSON(getPath, function (data) {
    if (data.length > 0) {
      for (var i = 0; i < data.length; i++) {
        var messageHTML = '<div class="pure-g-r message"><div class="pure-u-1"><div class="avatar"><img src="http://www.gravatar.com/avatar/96?s=32&amp;d=identicon&amp;r=PG" alt=""></div><div class="body"><div class="text">' + data[i].content + '</div><div class="details"><div class="pure-g-r"><div class="pure-u-1-2"> posted by ' + data[i].username + '</div><div class="pure-u-1-2 right">just now</div></div></div></div></div></div>';

        $('.main-inner').append(messageHTML);    
      }

      scrollToBottom();
    }
  });
}

$(document).ready(sectionsInit);
document.addEventListener('page:load', sectionsInit);