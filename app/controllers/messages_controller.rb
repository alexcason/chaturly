class MessagesController < ApplicationController
  before_action :set_section_title, :set_section

  def index
    @messages = @section.messages

    render json: @messages

    # respond_to do |format|
    #   format.json { results json: @messages }
    # end
  end

  def create
    # create message
    @message = @section.messages.build(message_params)

    respond_to do |format|
      if @message.save
        format.html { redirect_to @section }

        cookies[:username] = @message.username
      else
        format.html { redirect_to @section, flash: { error: 'Unable to create message.' } }
      end
    end
  end

  private
    def set_section_title
      @section_title = params[:section_id]
    end

    def set_section
      @section = Section.find_by_title(@section_title)

      puts 'Section'
      puts @section
    end

    def message_params
      params.require(:message).permit(:content, :username)
    end
end
