class SearchController < ApplicationController
  before_action :set_url, :set_message_details

  def index
    if valid?(@url) || valid?('http://' + @url)
      # remove unwanted parts and symbols
      encoded_query = @url.sub(/^https?\:\/\//, '').sub(/^www./, '').gsub(/\//, ' ').gsub(/\./, ' ').to_url

      redirect_to section_path(encoded_query, :url => @url)
    else
      redirect_to root_path, flash: { :error => @url + ' is not a valid URL' }
    end
  end

  def feedback
    puts 'feedback'
    if valid?(@url) || valid?('http://' + @url)
      puts 'valid'
      # remove unwanted parts and symbols
      encoded_query = @url.sub(/^https?\:\/\//, '').sub(/^www./, '').gsub(/\//, ' ').gsub(/\./, ' ').to_url

      puts 'encoded'

      redirect_to section_path(encoded_query, {:url => @url, :username => @username, :content => @content, :vote => @vote})
    end
  end

  private
    def set_url
      @url = params[:url]
    end

    def set_message_details
      @username = params[:username]
      @content = params[:content]
      @vote = params[:vote]
    end

    def valid?(url)
      uri = URI.parse(url)
      uri.kind_of?(URI::HTTP)
    rescue URI::InvalidURIError
      false
    end

    def message?
      !@username.nil? && !@content.nil?
    end
end
