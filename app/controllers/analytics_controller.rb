class AnalyticsController < ApplicationController
  before_action :set_title, :set_section
  def index
  end

  private
    def set_title
      @title = params[:section_id]
    end

    def set_section
      @section = Section.find_by_title(@title)
    end
end
