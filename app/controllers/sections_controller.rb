class SectionsController < ApplicationController
  before_action :set_title, :set_section, :set_message_details

  def show
    @message = Message.new

    if !@username.nil? && !@message.nil? && !@vote.nil?
      puts 'add message'

      message = Message.new
      message.section = @section
      message.username = @username
      message.content = @content

      if message.save
        render nothing: true
      end
    end
  end

  private
    def set_title
      @title = params[:id]
    end

    def set_section
      if @title.blank?
        redirect_to root_path
      else
        @section = Section.find_by_title(@title)

        # create section if does not exist
        if @section.nil? 
          url = params[:url]

          if url.blank?
            redirect_to root_path
          else
            @section = Section.new
            @section.title = @title
            @section.url = url
            @section.save

            params.delete :url
          end
        end
      end
    end

    def set_message_details
      @username = params[:username]
      @content = params[:content]
      @vote = params[:vote]
    end
end