class Section < ActiveRecord::Base
  has_many :messages

  def to_param
    title # or whatever you set :url_attribute to
  end
end
